//EDIT//
def service = 'pplusweb-front-api-other'


pipeline {
    environment {
        dockerhubiden = 'dockerid'
        imagename = ''
    }

    agent any
        stages {
            stage('prepare') {
                steps {
                    echo "prepare env"
                    sh 'printenv'
                    script {
                        imagename = "arm2017/go-test-build:v${BUILD_NUMBER}"
                    }
                }
            }

            stage('build code') {
                agent {
                    docker { 
                        image 'golang:1.19.0-alpine3.16' 
                        reuseNode true
                        // registryUrl 'https://myregistry.com/'
                        // registryCredentialsId 'myPredefinedCredentialsInJenkins'
                    }
                }
                steps {
                    echo "build code"
                    sh 'go version'
                    sh 'GOCACHE=/tmp/ go build -o build/'
                }
            }

             stage('build image') {
                steps {
                    echo "build image"
                    sh 'ls -l build/'
                    echo "${imagename}"
                    script {
                        def image = docker.build(imagename)

                        docker.withRegistry('', dockerhubiden) {
                            image.push()
                        }
                        
                    }

                }
             }
    }
    post {
        always {
            deleteDir()
        }
    }
}

@NonCPS
def getfullimagetag(deployment,regex){
    def match = deployment =~ regex
    match.find()
    fullimagetag =  match[0][1]
    return fullimagetag
}
